-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2019 at 11:40 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`) VALUES
('BR001', 'Laptop Asus Core i5', 4500000, 'JN002', 1),
('BR006', 'Natrium Hipoklorit', 450, 'JN004', 1),
('BR007', 'Printer Canon', 700000, 'JN00', 1),
('BR043', 'Natrium Hipoklorit', 450000, 'JN004', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'Junior Programmer', 'Programmer', 1),
('JB007', 'Staff', 'Ketua', 1),
('JB009', 'Junior Programmer', 'Programmer', 1),
('JB019', 'Office Boy', 'Oprasional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Perangkat Keras', 1),
('', '', 1),
('JN003', 'Perangkat Lunak', 1),
('JN004', 'Bahan Kimia', 1),
('JN001', 'Alat Tulis Kantor', 1),
('JN008', 'Hardware', 1),
('JN008', 'Software', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`) VALUES
('1902001', 'Putri Ayu', 'Jakarta', '0000-00-00', 'P', 'Pasar Kaget, Tj.Priok - Jakarta Utara', '', 'JB003', 1),
('1902003', 'Langgeng Kumboro', 'Lebak', '1998-12-08', 'L', 'RSN Waduk Pluit No. 302\r\nKec. Penjarngan - Jakarta Utara', '0815 1124 7338', 'JB003', 1),
('1902008', 'Rahmad', 'Yogyakarta', '1995-03-01', 'L', 'Jl. Merdeka Barat', '0872 1723 1921', 'JB007', 1),
('1902010', 'Chuang Le San Min', 'Beijing', '0000-00-00', 'L', 'Kemang ', '', 'JB003', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'CV. Informa Abadi', 'Jl. Mangga Dua Raya No. 10', '0213383838', 1),
('SP002', 'PT. Amarta Indah Otsuka', 'Pasuruan 67172, Jawa Timur', '08001687852', 1),
('SP004', 'PT. Senosa Makmur Tbk', 'Jl. Raya Gadog 12', '021 0291 9912', 1),
('SP009', 'Samsung Indonesia', 'Cikarang', '021 7777 9999', 1),
('SP011', 'PT. Garda Terdepan Utama', 'Jl. Mangga Besar Kav. 9A', '021 778 775', 1),
('SP012', 'PT. Garda Terdepan', 'Jl. Mangga Besar Kav. 9C', '0877 8191 9013', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
