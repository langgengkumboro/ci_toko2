<?php
class Jenisbarang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Jenisbarang_models");
	}

	public function index()
	{
		$this->data_jenisbarang();
	}
	public function data_jenisbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();
		$this->load->view('data_jenisbarang', $data);
	}

	public function detailjenisbarang($kode_jenis)
		{
			$data['data_jenisbarang'] =$this->Jenisbarang_models->detailjenisbarang($kode_jenis);
		    $this->load->view('detailjenisbarang', $data);
		}
	
	public function inputjenisbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();

		if (!empty($_REQUEST)){
			$m_jenisbarang = $this->Jenisbarang_models;
			$m_jenisbarang->save();
			redirect("Jenisbarang/index", "refresh");
		}


		$this->load->view('inputjenisbarang',$data);
	}
	
	public function editjenisbarang($kode_jenis)
	{	
		$data['data_jenisbarang']	= $this->Jenisbarang_models->detailjenisbarang($kode_jenis);
		
		if (!empty($_REQUEST)) {
				$m_jenisbarang = $this->Jenisbarang_models;
				$m_jenisbarang->update($kode_jenis);
				redirect("Jenisbarang/index", "refresh");	
			}
		
		$this->load->view('editjenisbarang', $data);	
	}

	public function delete($kode_jabatan)
	{
		$m_jenisbarang = $this->Jenisbarang_models;
		$m_jenisbarang->delete($kode_jenis);
		redirect("Jenisbarang/index", "refresh");	
	}

}

