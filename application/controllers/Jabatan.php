<?php
class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Jabatan_models");
	}

	public function index()
	{
		$this->data_jabatan();
	}
	public function data_jabatan()
	{
		$data['data_jabatan'] = $this->Jabatan_models->tampilDataJabatan();
		$this->load->view('data_jabatan', $data);
	}
	public function detailjabatan($kode_jabatan)
	{
		$data['data_jabatan'] =$this->Jabatan_models->detail($kode_jabatan);
		$this->load->view('detailjabatan', $data);
	}

	public function inputdatajabatan()
	{
		$data['data_jabatan'] = $this->Jabatan_models->tampilDataJabatan();

		if (!empty($_REQUEST)){
			$m_jabatan = $this->Jabatan_models;
			$m_jabatan->save();
			redirect("Jabatan/index", "refresh");
		}


		$this->load->view('inputdatajabatan',$data);
	}
	public function editjabatan($kode_jabatan)
	{	
		$data['data_jabatan'] = $this->Jabatan_models->detail($kode_jabatan);
		
		if (!empty($_REQUEST)) {
				$m_jabatan = $this->Jabatan_models;
				$m_jabatan->update($kode_jabatan);
				redirect("Jabatan/index", "refresh");	
			}
		
		$this->load->view('editjabatan', $data);	
	}

	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->Jabatan_models;
		$m_jabatan->delete($kode_jabatan);	
		redirect("Jabatan/index", "refresh");	
	}



}

