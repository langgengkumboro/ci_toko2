<table width="100%" border="0">
  <tr>
    <th scope="row">DETAIL</th>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
  </tr>
  <?php
        foreach ($data_detail as $data) 
        {
         
      ?>
  <tr>
    <th scope="row"><table width="78%" border="0">
      <tr>
        <th width="50%" scope="row">NIK</th>
        <td>:</td>
        <td width="49%"><?= $data->nik; ?></td>
      </tr>
      <tr>
        <th scope="row">NAMA</th>
        <td>:</td>
        <td><?= $data->nama_lengkap; ?></td>
      </tr>
      <tr>
        <th scope="row">TEMPAT LAHIR</th>
        <td>:</td>
        <td><?= $data->tempat_lahir; ?></td>
      </tr>
      <tr>
        <th scope="row">TANGGAL LAHIR</th>
        <td>:</td>
        <td><?= $data->tgl_lahir; ?></td>
      </tr>
      <tr>
        <th scope="row">JENIS KELAMIN</th>
        <td>:</td>
        <td><?= $data->jenis_kelamin; ?></td>
      </tr>
      <tr>
        <th scope="row">ALAMAT</th>
        <td>:</td>
        <td><?= $data->alamat; ?></td>
      </tr>
      <tr>
        <th scope="row">NO TELP</th>
        <td>:</td>
        <td><?= $data->telp; ?></td>
      </tr>
      <tr>
        <th scope="row">NAMA JABATAN</th>
        <td>:</td>
        <td><?= $data->nama_jabatan; ?></td>
      </tr>
    </table></th>
  </tr>
  <?php 
        } 
    ?>

</table>
