<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenisbarang_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "jenis_barang";

	public function tampilDataJenisbarang()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataJenisbarang2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM jenis_barang WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataJenisbarang3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_jenis', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			
			$data['kode_jenis'] =$this->input->post('kode_jenis');
			$data['nama_jenis'] =$this->input->post('nama_jenis');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
		}
	public function detailjenisbarang($kode_jenis)
		{
			$this->db->select('*');
			$this->db->where('kode_jenis', $kode_jenis);
			$this->db->where('flag', 1);
			$result = $this->db->get($this->_table);
			return $result->result();
		}

	public function delete($kode_jenis)
	
		{
			$this->db->where('kode_jenis',$kode_jenis);
			$this->db->delete($this->_table);
		}

} 